//create getCube variable and use exponent opertator 
let num = 2
let getCube = num ** 3
//print using template literals
console.log(`The cube of 2 is ${getCube}.`)

//create variable address with value of array
let address = ["258 Washington Ave NW", "California", 90011];
//destructure array
const [streetName, state, zip] = address;
//print message
console.log(`I live at ${streetName} ${state} ${zip}.;`)


//create var animal with value obj with diff animal details
const animal = {
    name: 'Muning',
    type: 'mythical beast',
    weight: 10,
    height: '2 ft 5 in',

};
//destructure the object
const {name, type, weight, height } = animal;

//print message
console.log(`${name} was a ${type}. He weighted at ${weight} lbs with a measurement of ${height}.`)


//create an array of numbers

const numbersA = [1, 2, 3, 4, 5];
//loop using foreach() add arrow function return
numbersA.forEach((number) => console.log(number));

//create class dog and constructor with name, age and breed
class Dog {
    constructor(name, age, breed) {
        this.name = name,
        this.age = age,
        this.breed = breed
    }
}
//instantiate a new object from class dog

const blackie = new Dog('Blackie', 10, 'very cute doggo');
console.log(blackie)